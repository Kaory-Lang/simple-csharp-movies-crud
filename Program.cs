﻿using System;
using System.Collections.Generic;
using Asignation_5.Database;
using Asignation_5.Database.Model;
using System.Linq;

namespace Asignation_5;

internal class Program {
    public static void Main(string[] args) {
        
        // Menu Interaction
        string menu(List<string> options) {
            string result = "";
            string input;
            
            for(int index = 0; index < options.Count; index++)
                Console.Write($"[{index}] {options[index]}\n");
            
            while(result == "") {
                try {
                    Console.Write("Insert Input>> ");
                    input = Console.ReadLine();
                    int index = Convert.ToInt32(input);
                    result = options[index];
                } catch {
                    Console.WriteLine("ERROR!!! Invalid Input");
                }
            }

            return result; 
        }

        string decorator(String label) {
            int space = 14;
            int charCount = label.Trim().Length;
            String result = "";
 
            void addSpace(int number) {
                for(int x = 0; x < number; x++) result += ' ';
            }
 
            if(charCount % 2 != 0) {
                addSpace(((space-charCount)/2) + 1);
                result += label.Trim();
                addSpace(((space-charCount)/2) + 1);
            } else {
                addSpace(((space-charCount)/2) + 1);
                result += label.Trim();
                addSpace((space-charCount)/2);
            }
 
            return result + '|';
        }


        // Generic Controllers
        void create(string dbTarget) {
            MoviesDBContext db = new MoviesDBContext();

            Console.Write($"Insert {dbTarget} Name>> ");
            string name = Console.ReadLine();

            if(dbTarget == "Gender") {
                Gender gender = new Gender() {
                    GenderName = name
                };
                db.Genders.Add(gender);
                db.SaveChanges();
            } else if(dbTarget == "Movie") {
                Console.Write("Insert Gender ID of the Movie>> ");
                string input = Console.ReadLine();

                int genderId = 0;
                try {
                    genderId = Convert.ToInt32(input);
                } catch {
                    Console.WriteLine("Incorrect Input. Only Digits");
                }

                Movie movie = new Movie() {
                    MovieName = name,
                    GenderId = genderId
                };
                db.Movies.Add(movie);
                db.SaveChanges();
            }
            
        }

        void read(string dbTarget) {
            MoviesDBContext db = new MoviesDBContext();

            if(dbTarget == "Gender") {
                Console.Write(
                    decorator("Gender Id")
                    + decorator("GenderName")
                    + '\n');

                foreach(var row in db.Genders.ToList()) {
                    Console.WriteLine(
                        decorator(row.GenderId.ToString())
                        + decorator(row.GenderName)
                    );
                }
            } else if(dbTarget == "Movie") {
                Console.Write(
                    decorator("Movie Id")
                    + decorator("MovieName")
                    + decorator("Gender(id)")
                    + '\n'
                );

                foreach(var row in db.Movies.ToList()) {
                    Console.WriteLine(
                        decorator(row.MovieId.ToString())
                        + decorator(row.MovieName)
                        + decorator(
                            db.Genders.Find(row.GenderId).GenderName
                            + '(' + row.GenderId.ToString() + ')'
                        )
                    );
                }
            }
        }

        void update(string dbTarget) {
            int id = 0;
            Console.Write("Insert ID of the row to change>> ");
            MoviesDBContext db = new MoviesDBContext();
    
            try {
                id = Convert.ToInt32(Console.ReadLine());
            } catch {
                Console.WriteLine("ERROR!!! Insert Integer Number");
            }

            if(dbTarget == "Movie") {
                Movie movie = db.Movies.Find(id);
                Console.Write("insert new movie name >> ");
                movie.MovieName = Console.ReadLine();
                Console.Write("insert new gender id >> ");

                try {
                    movie.GenderId = Convert.ToInt32(Console.ReadLine());
                } catch {
                    Console.WriteLine("error!!! insert integer number");
                }
                
                db.SaveChanges();
            }
            else if(dbTarget == "Gender") {
                Gender gender = db.Genders.Find(id);
                Console.Write("Insert new Gender Name >> ");
                gender.GenderName = Console.ReadLine();
                db.SaveChanges();
            }
        }

        void delete(string dbTarget) {
            int id = 0;
            Console.Write("Insert ID of the row to change>> ");
            MoviesDBContext db = new MoviesDBContext();
    
            try {
                id = Convert.ToInt32(Console.ReadLine());
            } catch {
                Console.WriteLine("ERROR!!! Insert Integer Number");
            }

            if(dbTarget == "Movie") {
                Movie movie = db.Movies.Find(id);
                db.Movies.Remove(movie);
                db.SaveChanges();
            } else if(dbTarget == "Gender") {
                Gender gender = db.Genders.Find(id);
                db.Genders.Remove(gender);
                db.SaveChanges();
            }
            Console.WriteLine("Deleted");
        }
        
        // Main Flow
        List<string> optionsMainMenu = new List<string>();
        optionsMainMenu.Add("Movie");
        optionsMainMenu.Add("Gender");

        List<string> optionsCRUD = new List<string>();
        optionsCRUD.Add("Create");
        optionsCRUD.Add("Read");
        optionsCRUD.Add("Update");
        optionsCRUD.Add("Delete");

        Console.WriteLine("Select Table:");
        string dbTarget = menu(optionsMainMenu);
        Console.WriteLine("\nSelect CRUD Option");
        string task = menu(optionsCRUD); 

        try {
            if(task == "Create") create(dbTarget);
            else if(task == "Read") read(dbTarget);
            else if(task == "Update") update(dbTarget);
            else if(task == "Delete") delete(dbTarget);
        } catch {
            Console.WriteLine("An ocurred while doing the action");
        }
    }
}
