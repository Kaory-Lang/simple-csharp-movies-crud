using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Asignation_5.Database.Model;

public class Gender {
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int GenderId { get; set; }
    public string GenderName { get; set; }
    public virtual ICollection<Movie> Movies { get; set; }
}
