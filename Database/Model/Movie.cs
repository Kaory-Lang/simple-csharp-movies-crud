using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Asignation_5.Database.Model;

public class Movie {
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int MovieId { get; set; }
    public string MovieName { get; set; }
    public int GenderId { get; set; }

    [ForeignKey("GenderId")]
    public virtual Gender Gender { get; set; }
}
