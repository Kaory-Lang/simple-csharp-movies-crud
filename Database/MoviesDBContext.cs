using Microsoft.EntityFrameworkCore;
using Asignation_5.Database.Model;

namespace Asignation_5.Database;

public class MoviesDBContext : DbContext {
    public DbSet<Movie> Movies { get; set; }
    public DbSet<Gender> Genders { get; set; }
    
    protected override void OnModelCreating(ModelBuilder modelBuilder) {
        modelBuilder.Entity<Movie>().ToTable("Movie");
        modelBuilder.Entity<Gender>().ToTable("Gender");
    }

    protected override void OnConfiguring(DbContextOptionsBuilder options) {
        options.UseSqlServer("Server=localhost; Database=Asignation_5DB; User Id=SA; Password=Mssql123.;");
    }
} 
